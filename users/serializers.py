"""
Serializers for users/ API endpoint.
"""
from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from .models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Specification of response payload that is going to be sent to client when browsing users/ API enpoint.
    """
    posts = serializers.HyperlinkedIdentityField(
        'post-detail', many=True, read_only=True)
    comments = serializers.HyperlinkedIdentityField(
        'comment-detail', many=True, read_only=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('id', 'url', 'username', 'email', 'first_name', 'last_name', 'password',
                  'avatar', 'posts', 'comments', 'is_staff', 'date_updated', 'date_joined',)
        read_only_fields = ('url', 'id', 'avatar', 'posts',
                            'comments', 'is_staff', 'date_updated', 'date_joined')
        create_only_fields = ('username',)
        write_only_fields = ('password',)

    def create(self, validated_data):
        validated_data['password'] = make_password(
            validated_data.get('password'))
        return super(UserSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        # FIXME: updating create_only data, temporarily disallowed all updates
        create_only_validated_data = dict(validated_data.pop(x)
                                          for x in self.Meta.create_only_fields
                                          if x in validated_data)
        return super(UserSerializer, self).update(instance, create_only_validated_data)
