"""
Views for posts/ API endpoint.
"""
from rest_framework import viewsets, permissions
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework.authentication import (
    SessionAuthentication, BasicAuthentication,)
from blgog_rest.permissions import IsAdminOrReadOnly
from .models import Post
from .serializers import PostSerializer


class PostViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    """
    Permissions
    -----------
        Anonymous or authenticated user : Read only
        Admin : Allow all
    """
    queryset = Post.objects.all().order_by('-created_at')
    serializer_class = PostSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication,)
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly, IsAdminOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
