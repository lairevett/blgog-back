"""
Models for posts/ API endpoint.
"""
from django.db import models
from blgog_rest import settings


class Post(models.Model):
    """
    Database posts table scheme.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='posts',
                             on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return ('"%(title)s", created by %(user)s %(created_at_formatted)s' % {
            'title': self.title,
            'user': self.user,
            'created_at_formatted': self.created_at.strftime('on %a, %-d %h %Y at %l:%M:%S'),
        })
