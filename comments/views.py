"""
Views for comments/ API endpoint.
"""
from rest_framework import viewsets, permissions
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework.authentication import (
    SessionAuthentication, BasicAuthentication,)
from blgog_rest.permissions import IsOwnerOrReadOnly
from .models import Comment
from .serializers import CommentSerializer


class CommentViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    """
    Permissions
    -----------
        Anonymous : Read
        Authenticated user : Write
        Owner : Update and delete
    """
    queryset = Comment.objects.all().order_by('created_at')
    serializer_class = CommentSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication,)
    permission_classes = (
        permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
