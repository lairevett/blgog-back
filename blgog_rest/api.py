from rest_framework.routers import DefaultRouter
from rest_framework_extensions.routers import NestedRouterMixin
from posts.views import PostViewSet
from comments.views import CommentViewSet
from users.views import UserViewSet


class NestedDefaultRouter(NestedRouterMixin, DefaultRouter):
    pass


router = NestedDefaultRouter()

posts_router = router.register('posts', PostViewSet)
posts_router.register('comments', CommentViewSet,
                      base_name='post-comments', parents_query_lookups=['post']).register('user', UserViewSet, base_name='post-comment-user', parents_query_lookups=['comments__post', 'post'])
posts_router.register('user', UserViewSet,
                      base_name='post-user', parents_query_lookups=['posts'])

comments_router = router.register('comments', CommentViewSet)
comments_router.register(
    'post', PostViewSet, base_name='comment-post', parents_query_lookups=['comments'])
comments_router.register(
    'user', UserViewSet, base_name='comment-user', parents_query_lookups=['comments'])

users_router = router.register('users', UserViewSet)
users_router.register('posts', PostViewSet,
                      base_name='user-posts', parents_query_lookups=['user'])
users_router.register('comments', CommentViewSet,
                      base_name='user-comments', parents_query_lookups=['user'])
