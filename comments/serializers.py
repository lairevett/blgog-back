"""
Serializers for comments/ API endpoint.
"""
from rest_framework import serializers
from blgog_rest import settings
from posts.models import Post
from .models import Comment


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    """
    Specification of response payload that is sent to the client when browsing comments/ API endpoint.
    """
    user = serializers.HyperlinkedRelatedField(
        'user-detail', read_only=True)
    post = serializers.HyperlinkedRelatedField(
        'post-detail', queryset=Post.objects.all())

    class Meta:
        model = Comment
        fields = ('id', 'url', 'user', 'post', 'content',
                  'created_at', 'updated_at',)
