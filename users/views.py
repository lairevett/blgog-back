"""
ViewSets used in blgog_rest.urls' ROUTER registered for users/ API enpoint.
"""
from django.shortcuts import get_object_or_404
from rest_framework.decorators import list_route
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import viewsets, permissions, mixins, status
from rest_framework_extensions.mixins import NestedViewSetMixin
from rest_framework.authentication import (
    SessionAuthentication, BasicAuthentication,)
from blgog_rest.permissions import (
    IsAnonymousOrDisallowCreate, IsOwnerOrReadOnly, IsAdminOrDisallowUpdate,)
from .serializers import UserSerializer
from .models import User


class UserViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    """
    Permissions
    -----------
        Anonymous or authenticated user : Read only
        Owner : Allow read and delete, disallow update completely temporarily
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    authentication_classes = (SessionAuthentication, BasicAuthentication,)
    permission_classes = (IsAnonymousOrDisallowCreate,
                          IsOwnerOrReadOnly, IsAdminOrDisallowUpdate,)

    def list(self, request, *args, **kwargs):
        username = request.GET.get('username')
        email = request.GET.get('email')

        if username is not None:
            user = get_object_or_404(User, username=username)
        elif email is not None:
            user = get_object_or_404(User, email=email)
        else:
            return super().list(request, args, kwargs)

        return Response(UserSerializer(user, context={'request': request}).data, status=status.HTTP_200_OK)
