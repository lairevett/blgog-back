"""
Serializers for posts/ API endpoint views.
"""
from rest_framework import serializers
from .models import Post


class PostSerializer(serializers.HyperlinkedModelSerializer):
    """
    Specification of response payload that is going to be sent to client when browsing posts/ API enpoint.
    """
    user = serializers.HyperlinkedRelatedField(
        'user-detail', read_only=True)
    comments = serializers.HyperlinkedRelatedField(
        'comment-detail', many=True, read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'url', 'user', 'title', 'content',
                  'comments', 'created_at', 'updated_at',)
