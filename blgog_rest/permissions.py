"""
Permissions module for views.
"""
from rest_framework import permissions


class IsAnonymousOrDisallowCreate(permissions.BasePermission):
    """
    If user is authenticated then revoke access to POST method.
    """

    def has_permission(self, request, view):
        if request.method == 'POST':
            return not request.user.is_authenticated

        return True


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    If currently logged in user isn't owner of the account, then allow GET, POST, HEAD and OPTIONS, otherwise allow all REST methods.
    """

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj == request.user


class IsAdminOrReadOnly(permissions.BasePermission):
    """
    Grant access to only read methods if user is not staff, if user is admin grant access to all methods.
    """

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        return request.user.is_staff


class IsAdminOrDisallowUpdate(permissions.BasePermission):
    """
    Grant access to PUT and PATCH request only if user is staff.
    """

    def has_object_permission(self, request, view, obj):
        if request.method == 'PUT' or request.method == 'PATCH':
            return request.user.is_staff

        return True
