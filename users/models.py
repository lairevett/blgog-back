"""
Models and helper methods for users/ API endpoint.
"""
from django.db import models
from django.contrib.auth.models import AbstractUser


def avatar_upload_path(instance, filename):
    """
    Returns user's avatar upload path: storage/avatars/user_<int:pk>/<str:filename>.
    """
    return f'strorage/avatars/user_{instance.user.id}/{filename}'


class User(AbstractUser):
    """
    Extension of User model.
    """
    email = models.EmailField(max_length=100)
    avatar = models.ImageField(
        upload_to=avatar_upload_path, blank=True, null=False)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        swappable = 'AUTH_USER_MODEL'
        db_table = 'users_user'
