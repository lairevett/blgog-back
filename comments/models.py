"""
Models for comments/ API endpoint.
"""
from django.db import models
from blgog_rest import settings
from posts.models import Post


class Comment(models.Model):
    """
    Database scheme for comments table.
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='comments', on_delete=models.CASCADE)
    post = models.ForeignKey(
        Post, related_name='comments', on_delete=models.CASCADE)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
