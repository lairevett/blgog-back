"""
All urls from endpoints + root.
"""
from django.urls import path, include
from .api import router

urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include(router.urls))
]
